---
layout: post
title: wpf manual
permalink: /wpf_manual/
---

* content
{:toc}


System architecture
------------------------
<br>

<img src="{{ '/styles/images/wpf/SystemArchitecture.PNG' | prepend: site.baseurl }}" />

<br>


System Login
------------------------
<br>
<ol>
  <li> <a href="{{ '/wpf_siteSetting' | prepend: site.baseurl }} ">단말기설정</a> </li>
  <li> <a href="{{ '/wpf_user' | prepend: site.baseurl }} ">사용자선택</a> </li>
  <li> <a href="{{ '/wpf_login' | prepend: site.baseurl }}">사용자로그인</a> </li>
  <li> <a href="{{ '/wpf_equipment' | prepend: site.baseurl }}" >라인선택</a> </li>
</ol>


System Main 
------------------------
<br>
<ol>
  <li> <a href="{{ '/wpf_main' | prepend: site.baseurl }} ">메인화면</a> </li>
</ol>
<br>


System Menu 
------------------------
<br>
<ol>
  <li> <a href="{{ '/wpf_line' | prepend: site.baseurl }} ">라인변경</a> </li>
  <li> <a href="{{ '/wpf_workequipment' | prepend: site.baseurl }} ">근무표변경</a> </li>
  <li> <a href="{{ '/wpf_labelprint' | prepend: site.baseurl }} ">라벨발행</a> </li>
  <li> <a href="{{ '/wpf_bom' | prepend: site.baseurl }} ">작업준비</a> </li>
</ol>
<br>


User Config
------------------------
<br>
<ol>
  <li> <a href="{{ '/wpf_grid' | prepend: site.baseurl }} ">그리드설정</a> </li>
  <li> <a href="{{ '/wpf_config' | prepend: site.baseurl }} ">환경설정</a> </li>
  <li> <a href="{{ '/wpf_fullscreen' | prepend: site.baseurl }} ">꽉찬화면</a> </li>
</ol>
<br>


Tracking
------------------------
<br>
<ol>
  <li> <a href="{{ '/wpf_workstart' | prepend: site.baseurl }} ">작업시작</a> </li>
  <li> <a href="{{ '/wpf_workresult' | prepend: site.baseurl }} ">작업완료</a> </li>
  <li> <a href="{{ '/wpf_manualwork' | prepend: site.baseurl }} ">수작업실적</a> </li>
  <li> <a href="{{ '/wpf_faultproduct' | prepend: site.baseurl }} ">제품불량</a> </li>
</ol>
<br>

