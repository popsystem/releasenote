---
layout: post
title: 사이트 설정
permalink: /wpf_siteSetting/
---

* content
{:toc}


사이트 설정
------------------------
<br>

<img src="{{ '/styles/images/wpf/site/01.PNG' | prepend: site.baseurl }}" />

<br>


언어
------------------------
<br>
<ol>
  <li>KOR: 한국어 </li>
  <li>ENG: 영어 </li>
</ol>
<br>

사이트 코드
------------------------
<br>
<blockquote><p> SmartMOM 설정 된 사이트 코드를 입력합니다. </p></blockquote>
<br>

프로토콜
------------------------
<br>
<blockquote><p> WebService 통신 <a href="http://terms.naver.com/entry.nhn?docId=856477&cid=42346&categoryId=42346">프로토콜</a>을 지정합니다. </p></blockquote>
<br>


서버주소
------------------------
<br>
<blockquote><p> 서버IP 또는 도메인을 입력합니다. </p></blockquote>
<br>
