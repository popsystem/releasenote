---
layout: post
title: 환경설정
permalink: /wpf_config/
---

* content
{:toc}


환경설정
------------------------
<br>

<img src="{{ '/styles/images/wpf/config/01.PNG' | prepend: site.baseurl }}" />

<br>


일반설정
------------------------
<br>
 
<img src="{{ '/styles/images/wpf/config/02.PNG' | prepend: site.baseurl }}" />

<br>
<ol>
  <li> 언어: KOR( 한국어 ) / ENG ( 영어 ) </li>
  <li> 시작화면: POP시작화면 </li>
  <li> 그리드크기: 그리드 컬럼 폰트 크기 </li>
  <li> 라벨크기: 화면 라벨폰트 크기 </li>
  <li> 표시시간: POPUP 메시지 표시 시간(초) </li>
  <li> MOM사이트: MOM링크 사이트 주소 </li>
  <li> 갱신시간(초): Background 처리 시 갱신시간 설정 </li>
  <li> 사전로그인(시간): 라인 셋업시간 </li>
  <li> OPC여부: OPC 설치 여부 </li>
  <li> 비가동: 라인 비가동 여부 </li>
  <li> 자재투입: 라인 자재투입 여부 </li>
</ol>

제품라벨
------------------------
<br>

<img src="{{ '/styles/images/wpf/config/03.PNG' | prepend: site.baseurl }}" />

<br>
<ol>
  <li> 사용여부: 제품라벨 사용여부 </li>
  <li> 라벨분할: 제품라벨 수량 분할여부 </li>
  <li> 묶음발행: 동일품번 라벨출력 여부 </li>
  <li> 인쇄방식: NOTUSE( 없음 ) / EACH_LABEL( 개별라벨 ) / WORKORDERLABEL( LOT라벨 )  </li>
  <li> 인쇄타입: PrinterName( 프린터명 ) / TCPIP ( 네트워크 ) </li>
  <li> 제조사: 라벨프린터 제조사 </li>
  <li> 라벨프린터: 인쇄 프린터 </li>
  <li> IP주소: 네트워크 인쇄 시 프린터에 설정된 IP 주소 </li>
  <li> 포트번호: 네트워크 인쇄 시 프린터에 설정된 포트 정보 </li>
</ol>


박스라벨 
------------------------
<br>

<img src="{{ '/styles/images/wpf/config/04.PNG' | prepend: site.baseurl }}" />

<br>
<ol>
  <li> 사용여부: 라벨라벨 사용여부 </li>
  <li> 포장방식: NOBOXPACKING( 포장안함 ) / SINGLEBOXPACKING( 단일포장 ) </li>
  <li> 인쇄타입: PrinterName( 프린터명 ) / TCPIP ( 네트워크 ) </li>
  <li> 스캔타입: NOTUSE( 사용안함 ) / PARTNO( 품번 ) / PRODUCTLABEL( 제품라벨 ) </li>
  <li> 제조사: 라벨프린터 제조사 </li>
  <li> 라벨프린터: 인쇄 프린터 </li>
  <li> IP주소: 네트워크 인쇄 시 프린터에 설정된 IP 주소 </li>
  <li> 포트번호: 네트워크 인쇄 시 프린터에 설정된 포트 정보 </li>
</ol>


소리설정
------------------------
<br>

<img src="{{ '/styles/images/wpf/config/05.PNG' | prepend: site.baseurl }}" />

<br>
<ol>
  <li> 성공소리: 정상처리 시 소리파일 </li>
  <li> 정보소리: 정보표시 소리파일 </li>
  <li> 에러소리: 에러처리 시 소리파일 </li>
</ol>


