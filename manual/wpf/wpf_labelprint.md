---
layout: post
title: 라벨발행
permalink: /wpf_labelprint/
---

* content
{:toc}


라벨발행
------------------------
<br>

<img src="{{ '/styles/images/wpf/labelprint/01.PNG' | prepend: site.baseurl }}" />

<br>


그리드
------------------------
<br>
<ol>
  <li> 체크박스: 라벨발행 대상 선정 </li>
  <li> 순번: 라벨 순번 </li>
  <li> 바코드: 라벨 바코드 </li>
  <li> 량: 라벨 수량 </li>
  <li> 아이템: 라벨 품번 </li>
  <li> 아이템명: 라벨 품명 </li>
  <li> 작업지시서: 라벨 작업지시서 </li>
  <li> 실적처리번호: 라벨 실적여부 </li>
  <li> 바코드타입: PRD( 제품라벨 ) / BOX( 박스라벨 ) </li>
</ol>
<br>


발행모드
------------------------
<br>
<blockquote><p> 라벨 생성설정 값 표 </p></blockquote>
<br>
<ol>
  <li> EACH_LABEL: 작업지시서 수량만큼 출력됩니다. </li>
  <li> WORKORDER_LABEL: 라벨1장만 출력됩니다. </li>
  <li> PACKING_LABEL: 포장단위(표준포장수량)로 출력됩니다. </li>
</ol>
<br>


조회버튼 
------------------------
<br>
<blockquote><p> 생성된 라벨 조회합니다. </p></blockquote>
<br>


저장버튼 
------------------------
<br>
<blockquote><p> 라벨을 생성합니다. </p></blockquote>
<br>


인쇄버튼 
------------------------
<br>
<blockquote><p> 라벨을 출력합니다. </p></blockquote>
<br>

