---
layout: post
title: 작업준비
permalink: /wpf_bom/
---

* content
{:toc}


BOM( Bill of Material )
------------------------
<br>

<img src="{{ '/styles/images/wpf/bom/01.PNG' | prepend: site.baseurl }}" />

<br>


그리드
------------------------
<br>
<blockquote><p> 선택 작업지시서 기준 준비자재 리스트 </p></blockquote>
<br>


인쇄버튼
------------------------
<br>

<img src="{{ '/styles/images/wpf/bom/02.PNG' | prepend: site.baseurl }}" />

<br>
<blockquote><p> 미리보기 확인 후 인쇄 </p></blockquote>
