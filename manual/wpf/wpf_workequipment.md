---
layout: post
title: 근무표
permalink: /wpf_workequipment/
---

* content
{:toc}


사용자선택
------------------------
<br>

<img src="{{ '/styles/images/wpf/workequipment/01.PNG' | prepend: site.baseurl }}" />

<br>


상단정보
------------------------
<br>
<ol>
  <li> DAY: 근무조 </li>
  <li> 2017-07-12: 근무일 </li>
</ol>
<br>


직접정보
------------------------
<br>
<blockquote><p> 제품 생산에 직접적인 투입공수 </p></blockquote>
<br>
<ol>
  <li> 정상: 정상근무 </li>
  <li> 잔업: 연장근무 </li>
</ol>
<br>


간접정보 
------------------------
<br>
<blockquote><p> 제품 생산에 간접적인 투입공수 </p></blockquote>
<br>
<ol>
  <li> 정상: 정상근무 </li>
  <li> 잔업: 연장근무 </li>
</ol>
<br>


확인버튼
------------------------
<br>
<blockquote><p> 근무일 기준 입력정보 저장합니다. </p></blockquote>
<br>

