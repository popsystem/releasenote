---
layout: post
title: 사용자선택
permalink: /wpf_user/
---

* content
{:toc}


사용자선택
------------------------
<br>

<img src="{{ '/styles/images/wpf/user/01.PNG' | prepend: site.baseurl }}" />

<br>


부서
------------------------
<br>
> 선택 사이트의 등록 된 부서정보 


그리드
------------------------
<br>

<img src="{{ '/styles/images/wpf/user/02.PNG' | prepend: site.baseurl }}" />

<br>
<blockquote><p> 사용자명 선택 시 로그인화면 이동  </p></blockquote>


종료
------------------------
<br>
<blockquote><p> POP 프로그램 종료  </p></blockquote>
<br>


