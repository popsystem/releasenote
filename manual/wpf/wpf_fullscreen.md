---
layout: post
title: 사용자선택
permalink: /wpf_fullscreen/
---

* content
{:toc}


꽉찬화면
------------------------
<br>

<img src="{{ '/styles/images/wpf/fullscreen/01.PNG' | prepend: site.baseurl }}" />

<br>
<blockquote><p> 윈도우 작업표시줄 또는 제목표시줄 까지 꽉찬화면으로 표시합니다. </p></blockquote>



