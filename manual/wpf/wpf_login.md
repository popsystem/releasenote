---
layout: post
title: 사용자로그인
permalink: /wpf_login/
---

* content
{:toc}


사용자로그인
------------------------
<br>

<img src="{{ '/styles/images/wpf/login/01.PNG' | prepend: site.baseurl }}" />

<br>


비밀번호
------------------------
<br>

<img src="{{ '/styles/images/wpf/login/02.PNG' | prepend: site.baseurl }}" />

<br>
<blockquote><p> 비밀번호 끝의 화살표 선택 시 키패드가 열립니다. </p></blockquote>


키패드
------------------------
<br>

<img src="{{ '/styles/images/wpf/login/03.PNG' | prepend: site.baseurl }}" />

<br>
<blockquote><p> 키패드 번호키 누를 시 자동으로 입력됩니다. </p></blockquote>


근무조
------------------------
<br>

<img src="{{ '/styles/images/wpf/login/04.PNG' | prepend: site.baseurl }}" />

<br>
<br>

