---
layout: post
title: 라인선택
permalink: /wpf_equipment/
---

* content
{:toc}


라인정보 리스트
------------------------
<br>

<img src="{{ '/styles/images/wpf/equipment/01.PNG' | prepend: site.baseurl }}" />

<br>


라인선택
------------------------
<br>

<img src="{{ '/styles/images/wpf/equipment/02.PNG' | prepend: site.baseurl }}" />

<br>
<blockquote><p> 라인명 선택으로 화면 전환 됩니다. </p></blockquote>
<br>


작업일
------------------------
<br>

<img src="{{ '/styles/images/wpf/equipment/03.PNG' | prepend: site.baseurl }}" />

<br>
<blockquote><p> 근무표 편성이 되지 않을경우 "0001-01-01" 으로 표시가 되며, 라인선택이 되지 않습니다. </p></blockquote>

<br>
<br>

