---
layout: post
title: 메인화면
permalink: /wpf_main/
---

* content
{:toc}


메인
------------------------
<br>

<img src="{{ '/styles/images/wpf/main/01.PNG' | prepend: site.baseurl }}" />

<br>


제목표시줄
------------------------
<br>
<ol>
  <li> 아이콘: MOM사이트 방문 </li>
  <li> 사이트: POP 접속 사이트명 </li>
  <li> 전진근 주간: 접속 사용자명 + 근무조 </li>
  <li> 2017-07-12: 작업일 </li>
  <li> 블로그: POP 블로그방문 ( 업데이트 및 오류조치사항 공유 ) </li>
  <li> 메뉴얼: 선택 화면 메뉴얼 확인 </li>
  <li> 그리드설정: 선택화면 그리드 설정 </li>
  <li> 환경설정: POP 환경설정 </li>
  <li> 꽉찬화면: POP 화면을 꽉찬화면으로 표시 </li>
</ol>
<br>


메뉴버튼
------------------------
<br>
<blockquote><p> 메뉴를 표시합니다.  </p></blockquote>
<br>


선택작업지시서
------------------------
<br>
<blockquote><p> 작업지시서의 상세정보를 표시합니다.  </p></blockquote>
<ol>
  <li> 작업시작: 선택 화면 명 </li>
  <li> LINE4: 작업 라인 표시합니다. </li>
  <li> WM1704260001: 선택중인 작업지시서번호입니다. </li>
  <li> ABQ36680015: 작업지시서의 품번입니다. </li>
  <li> Case Assembly,Control: 품번의 품명입니다. </li>
</ol>
<br>


그리드
------------------------
<br>
<blockquote><p> 선택화면의 조회데이터를 표시합니다.  </p></blockquote>
<br>


조회조건
------------------------
<br>
<blockquote><p> 선택화면의 조회조건을 표시합니다.  </p></blockquote>
<br>



버튼
------------------------
<br>
<blockquote><p> 선택화면의 버튼정보를 표시합니다.  </p></blockquote>
<br>

