---
layout: post
title: 작업완료
permalink: /wpf_workresult/
---

* content
{:toc}


작업완료
------------------------
<br>

<img src="{{ '/styles/images/wpf/workresult/01.PNG' | prepend: site.baseurl }}" />

<br>


바코드그리드
------------------------
<br>

<img src="{{ '/styles/images/wpf/workresult/02.PNG' | prepend: site.baseurl }}" />

<br>


그리드
------------------------
<br>
<img src="{{ '/styles/images/wpf/workresult/03.PNG' | prepend: site.baseurl }}" />
<br>
<ol>
  <li> 직압지시서: 작업지시서번호 </li>
  <li> 우선순위: 작업 우선순위 </li>
  <li> 고객사라인: 작업지시서의 수요 고객사 라인정보 </li>
  <li> 고객사납기일: 작업지시서 수요 납기일 </li>
  <li> 시작시간: 작업 시작시간 </li>
  <li> 아이템: 작업지시서 아이템 </li>
  <li> 지시량: 작업지시서 지시량 </li>
  <li> 불량: 작업지시서 제품불량 </li>
</ol>
<br>


바코드
------------------------
<br>
<blockquote><p> 스캔 바코드 입력박스입니다. </p></blockquote>
<br>


정보
------------------------
<br>
<blockquote><p> 마지막 스캔 바코드 및 상태정보  </p></blockquote>
<br>


조회버튼
------------------------
<br>
<blockquote><p> 선택 라인 기준 작업지시서 정보조회 버튼입니다.  </p></blockquote>
<br>


수작업이동버튼
------------------------
<br>
<blockquote><p> 선택 작업지시서 수작업실적조회합니다.  </p></blockquote>
<br>


제품불량이동버튼
------------------------
<br>
<blockquote><p> 선택 작업지시서 기준 제품불량등록 화면이동합니다.  </p></blockquote>
<br>
