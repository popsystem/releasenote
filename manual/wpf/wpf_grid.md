---
layout: post
title: 사용자선택
permalink: /wpf_grid/
---

* content
{:toc}


그리드설정화면
------------------------
<br>

<img src="{{ '/styles/images/wpf/grid/01.PNG' | prepend: site.baseurl }}" />

<br>


선택화면
------------------------
<br>
<blockquote><p> 현재 표시되고 있는 화면정보 </p></blockquote>
<br>


이동모드
------------------------
<br>
<blockquote><p> 그리드 컬럼 표시 순번을 변경한다 </p></blockquote>
<br>


그리드
------------------------
<br>
<ol>
  <li> 메뉴: 선택된 화면ID </li>
  <li> 그리드: 그리드ID </li>
  <li> 컬럼타입: 컬럼 표시 방식 </li>
  <li> 컬럼: 컬럼ID </li>
  <li> 컬럼명: 컬럼명 </li>
  <li> 정렬: L( 왼쪽정렬 ) / C( 중앙정렬 ) / R( 오른쪽정렬 )</li>
  <li> 정렬: 그리드컬럼 선택 시 자동정렬 여부 </li>
  <li> 숨김: 컬럼 숨김여부 </li>
</ol>


확인버튼
------------------------
<br>
<blockquote><p> 설정값 적용 </p></blockquote>
<br>

