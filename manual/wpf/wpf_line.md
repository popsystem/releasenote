---
layout: post
title: 라인변경
permalink: /wpf_line/
---

* content
{:toc}


라인변경
------------------------
<br>

<img src="{{ '/styles/images/wpf/line/01.PNG' | prepend: site.baseurl }}" />

<br>



그리드
------------------------
<br>
<ol>
  <li> 라인: 라인코드 </li>
  <li> 라인명: 라인명 </li>
  <li> 라인분류: 공정 </li>
  <li> 작업일: 근무일 </li>
</ol>
<br>


라인선택
------------------------
<br>
<blockquote><p> 라인선택 시 라인변경 처리 됩니다. </p></blockquote>
<br>


라인변경불가
------------------------
<br>
<blockquote><p> 작업일이 "0001-01-01" 일경우 해당 작업계획이 없으므로, 변경불가 합니다. </p></blockquote>
<br>
