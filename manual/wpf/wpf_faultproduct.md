---
layout: post
title: 제품불량
permalink: /wpf_faultproduct/
---

* content
{:toc}


제품불량
------------------------
<br>

<img src="{{ '/styles/images/wpf/faultproduct/01.PNG' | prepend: site.baseurl }}" />

<br>


그리드
------------------------
<br>
<blockquote><p> 제품불량 등록 현황 리스트  </p></blockquote>
<br>


불량등록 정보
------------------------
<br>
<ol>
  <li> 시작시간 </li>
  <li> 종료시간 </li>
  <li> 작업일 </li>
  <li> 공수(분) </li>
  <li> 투입인원 </li>
  <li> 총공수 </li>
  <li> 불량  </li>
</ol>


조회버튼
------------------------
<br>
<blockquote><p> 선택된 작업지시서 기준 제품불량정보 재조회 한다. </p></blockquote>
<br>


확인버튼
------------------------
<br>
<blockquote><p> 입력된 제품불량정보 등록한다. </p></blockquote>
<br>
