---
layout: post
title: 수작업실적
permalink: /wpf_manualwork/
---

* content
{:toc}


사용자선택
------------------------
<br>

<img src="{{ '/styles/images/wpf/manualwork/01.PNG' | prepend: site.baseurl }}" />

<br>


그리드
------------------------
<br>
<ol>
  <li> 시작시간: 작업시작시간 </li>
  <li> 종료시간: 작업종료시간 </li>
  <li> 작업일: 작업지시서 작업일 </li>
  <li> 공수(분): 작업종료시간 - 작업시작시간 </li>
  <li> 투입인원: 투입인원 </li>
  <li> 총공수: 투입인원 X 공수(분) </li>
  <li> 양품량: 작업수량 </li>
</ol>
<br>


조회버튼
------------------------
<br>
<blockquote><p> 선택 작업지시서 기준 실적조회합니다.  </p></blockquote>
<br>


확인버튼
------------------------
<br>
<blockquote><p> 선택 작업지시서 기준 수작업실적 등록합니다.  </p></blockquote>
<br>

