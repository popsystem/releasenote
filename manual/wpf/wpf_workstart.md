---
layout: post
title: 작업시작
permalink: /wpf_workstart/
---

* content
{:toc}


사용자선택
------------------------
<br>

<img src="{{ '/styles/images/wpf/workstart/01.PNG' | prepend: site.baseurl }}" />

<br>


그리드
------------------------
<br>
<ol>
  <li> 직압지시서: 작업지시서번호 </li>
  <li> 우선순위: 작업 우선순위 </li>
  <li> 고객사라인: 작업지시서의 수요 고객사 라인정보 </li>
  <li> 고객사납기일: 작업지시서 수요 납기일 </li>
  <li> 시작시간: 작업 시작시간 </li>
  <li> 아이템: 작업지시서 아이템 </li>
  <li> 지시량: 작업지시서 지시량 </li>
  <li> 제품라벨: 작업지시서 제품라벨 인쇄화면 이동 </li>
</ol>
<br>


지시일
------------------------
<br>

<img src="{{ '/styles/images/wpf/workstart/02.PNG' | prepend: site.baseurl }}" />

<br>


조회버튼
------------------------
<br>
<blockquote><p> 선택 라인 기준 작업시작 대기중인 작업지시서를 조회합니다. </p></blockquote>
<br>


배치라벨이동버튼
------------------------
<br>
<blockquote><p> 선택 작업지시서의 아이템 기준 일괄 제품라벨 생성화면으로 이동합니다. </p></blockquote>
<br>

