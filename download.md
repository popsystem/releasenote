---
layout: post
title: download
permalink: /download/
---

* content
{:toc}


WPF
==============================
<blockquote><p>Windows POPSystem</p></blockquote>
<br>
<h2>사양정보</h2>
<ol>
  <li>OS: Windows7 이상</li>
  <li>Memory: 4Gb 이상 </li>
</ol><br>
<h5>Download: [INSTALL][wpf-url]</h5>
<br>
<br>

UWP
==============================
<blockquote><p>PC 및 태블릿, 휴대폰 등 다양한 기기를 지원합니다.</p></blockquote>
<h2></h2>
[INSTALL][uwp-url]
<br>
<h2>사양정보</h2>
<ol>
  <li>PC: Windows10 이상의 일반PC</li>
</ol><br>
<h5>Download: [INSTALL][uwp-url]</h5>
<br>
<br>

Android
==============================
<blockquote><p>PC 및 태블릿, 휴대폰 등 다양한 기기를 지원합니다.</p><br><p>(단, 일반PC에서 사용 시 Emulator 설치가 필요합니다. )</p></blockquote>
[INSTALL][android-url]
<br>
<h2>사양정보</h2>
<ol>
  <li>AndroidOS: Android 6.0 이상 ( 마시멜로 )</li>
</ol><br>
<h5>Download: [INSTALL][android-url]</h5>
<br>
<br>


 

[wpf-url]:      http://mom.thirautech.com:9000/pop/publish.htm
[uwp-url]:		
[android-url]:	
