---
layout: post
comments: true
title:  2017-06-21 업그레이드 내역
date:   2017-06-21 00:00:01
categories: 일산전자
tag: upgrade
---

* content
{:toc}


업그레이드
------------------------

<h2>환경설정</h2>
<br>
<blockquote><p>일반설정 - 시작화면 </p></blockquote>
<br>

<ol>
  <li>TRACKINVIEW : "작업투입" 화면으로 시작</li>
  <li>TRACKOUTVIEW : "작업완료" 화면으로 시작</li>
  <li><b>SINGLEBOXPACKINGVIEW</b> : "단일박스포장" 화면으로 시작</li>
</ol>
<br><p><b>SINGLEBOXPACKINGVIEW</b> 선택 </p>
<br>
<br>
<blockquote><p>박스라벨 - 포장방식 </p></blockquote>
<br>

<ol>
  <li>NOBOXPACKING : 박스포장하지 않음</li>
  <li><b>SINGBLEBOXPACKING</b> : 단일박스포장</li>
</ol>
<br><p><b>SINGBLEBOXPACKING</b> 선택 </p>
<br>
<blockquote><p>박스라벨 - 스캔방식 </p></blockquote>
<br>

<ol>
  <li>NOTUSE : 사용안함</li>
  <li><b>PARTNO</b> : PARTNO라벨스캔</li>
  <li>PRODUCTLABEL : 제품라벨 스캔</li>
</ol>
<br><p><b>PARTNO</b> 선택 </p>
<br>


단일박스포장
------------------------

<blockquote>
<p>1. 포장 대상 작업지시서 선택</p>
 - 포장 PARTNO 와 동일한 작업지시서를 선택
<p>2. 포장 제품라벨 스캔</p>
 - 포장수량 인식을 위한 포장라벨 스캔
<p>3. 포장 제품버튼 클릭</p>
 - 실제 포장재에 담긴 수량과 POP 포장수량 일치 시 포장버튼을 누른다.
<p>4. 포장박스라벨 출력</p>
 - 포장 라벨이 출력된다. ( 제품박스에 부착 )
</blockquote>
<br>

<br>

