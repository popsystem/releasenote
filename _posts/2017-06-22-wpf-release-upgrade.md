---
layout: post
comments: true
title:  2017-06-22 업그레이드 내역
date:   2017-06-22 00:00:01
categories: 일산전자
tag: upgrade
---

* content
{:toc}


비가동실적( 작업지시서 )
------------------------

<h2>비가동실적( 작업지시서 )</h2>
<br>
<blockquote><p> UI 변경 : 초기 조회 화면: 비가동 코드별 실적 횟수 표시 </p></blockquote>
<br>

<ol>
  <li>비가동 코드 클릭 시 비가동 실적 누적</li>
  <li>비가동 실적횟수 클릭 시 상세 실적 조회</li>
</ol>
<br>
<br>


단일박스포장
------------------------

<h2>단일박스포장</h2>
<br>
<blockquote><p> PARTNO 라벨을 인식하여 포장 버튼 누를 시 포장실적등록 화면 </p></blockquote>
<br>

<ol>
  <li>일반설정 - 박스라벨 에서 사용여부를 YES 설정( 필수 )</li>
  <li>일반설정 - 박스라벨 에서 포장방식을 PARTNO 설정( 필수 )</li>
</ol>
<br>
<br>

<br>

