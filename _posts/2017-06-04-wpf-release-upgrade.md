---
layout: post
comments: true
title:  2017-06-04 패치내역
date:   2017-06-04 00:00:01
categories: 공통
tag: upgrade
---

* content
{:toc}


변경
------------------------
<h2>배포서버 변경</h2>
<br>
<blockquote><p>소스관리 및 배포관리 일원화</p></blockquote>
<br>
<ol>
  <li><b>이전</b> : 각 사이트에서 직접 배포</li>
  <li><b>변경</b> : mom 서버에서 배포</li>
</ol>

<br>

<br>
<br>



업그레이드
------------------------

<h2>환경설정</h2>
<br>
<blockquote><p>사용자 환경설정 DB화</p></blockquote>
<br>
<ol>
  <li><b>이전</b> : 각 PC별 관리</li>
  <li><b>변경</b> : Database 화</li>
</ol>
<br>

<blockquote><p>사전 로그인 기능 추가</p></blockquote>
<br>
<ol>
  <li><b>이전</b> 근무표 기준 로그인 가능</li>
  <li><b>패치</b> 환경설정에서 사전 로그인 시간설정 기준</li>
</ol>
<br>

<h2>라벨출력</h2>
<br>
<blockquote><p>네트워크 출력 추가</p></blockquote>
<br>
<ol>
  <li><b>이전</b> USB를 통한 Image방식만 가능</li>
  <li><b>패치</b> 네트워크 상에 존재하는 라벨프린터 인쇄가능( ZEBRA 프린터기기만 가능 )</li>
</ol>
<br>

<br>
<br>

