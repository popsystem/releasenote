---
layout: post
comments: true
title:  2017-06-19 업그레이드 내역
date:   2017-06-19 00:00:01
categories: 공통
tag: upgrade
---

* content
{:toc}


업그레이드
------------------------

<h2>환경설정</h2>
<br>
<blockquote><p>일반설정 - 비가동 </p></blockquote>
<br>

<ol>
  <li><b>NOTUSE</b> : 비가동 없음</li>
  <li><b>EQUIPMENT</b> : 라인 기준 비가동</li>
  <li><b>WORKORDER</b> : 작업지시서 기준 비가동</li>
</ol>
<br>
<br>
<h3>비가동코드정의</h3>
<blockquote><p>코드관리</p></blockquote>
<br>
<ol>
  <li><b>코드그룹</b> : CONTROLLABLE</li>
  <li><b>코드아이디</b> : 비가동 코드</li>
</ol>


<h2>메인화면</h2>
<br>
<blockquote><p>블로그</p></blockquote>
<br>
<h3>POPSystem 업그레이드 내용 및 패치내용 공유</h3>
<br>
<br>
