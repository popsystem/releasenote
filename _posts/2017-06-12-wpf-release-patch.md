---
layout: post
comments: true
title:  2017-06-12 패치내역
date:   2017-06-12 00:00:01
categories: 패치
tag: patch
---

* content
{:toc}

패치내역
------------------------
<ol>
  <li>메뉴에서 <b>라인변경</b> 선택 시 오류로인한 프로그램 종료현상 문제해결</li>
  <li><b>작업완료</b> 화면에서 모든 버튼이 <b>조회</b> 으로 표현되는 현상 문제해결</li>
</ol>

