---
layout: post
comments: true
title:  2017-06-12 업그레이드 내역
date:   2017-06-12 00:00:01
categories: 공통 DK전자 일산전자
tag: upgrade
---

* content
{:toc}


업그레이드
------------------------

<h2>환경설정</h2>
<br>
<blockquote><p>일반설정 - 시작화면</p></blockquote>
<br>

<ol>
  <li><b>TRACKINVIEW</b> : "작업투입" 화면으로 시작</li>
  <li><b>TRACKOUTVIEW</b> : "작업완료" 화면으로 시작</li>
</ol>
<br>
<br>

<blockquote><p>제품라벨 - 스캔방식</p></blockquote>
<br>

<ol>
  <li><b>NOTUSE</b> : 사용안함</li>
  <li><b>PARTNO</b> : 제품코드</li>
  <li><b>PRODUCTLABEL</b> : 제품라벨</li>
</ol>
<br>

<ul>
  <li><b>DK전자</b> : PRODUCTLABEL으로 설정하시기 바랍니다. </li>
  <li><b>일산전자</b> : DP / EMS 인 경우 PARTNO 으로 설정하시기 바랍니다. </li>
</ul>
<br>
<br>

