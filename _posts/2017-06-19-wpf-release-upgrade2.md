---
layout: post
comments: true
title:  2017-06-19 업그레이드 내역
date:   2017-06-19 00:00:01
categories: 일산전자
tag: upgrade
---

* content
{:toc}


비가동 관리
------------------------

<h2>정의: 작업지시서 기준 비가동 관리</h2>
<br>

<blockquote><p>1. 비가동 코드 등록</p></blockquote>
<br>


<blockquote><p>2. 비가동 실적 등록</p></blockquote>
<br>

라인알람 관리
------------------------

<blockquote><p>1. 작업지시서 기준 비가동 알람확인</p></blockquote>
<br>


<blockquote><p>2. 작업지시서 기준 비가동 알람해제</p></blockquote>
<br>

