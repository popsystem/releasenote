---
layout: post
comments: true
title:  2017-07-09 업그레이드 내역
date:   2017-07-09 00:00:01
categories: 공통
tag: upgrade
---

* content
{:toc}


기능 업데이트
------------------------

<h2> 단말기 설정정보 관리방식 변경 </h2>
<br>
<blockquote><p> .Config => xml 파일 변경  </p></blockquote>
<br>
<br>

<h2>언어 적용</h2>
<br>
<blockquote><p> 언어 변경 시 UI 즉시 적용  </p></blockquote>
<br>
<br>

<h2> 환경설정 CODE </h2>
<br>
<blockquote><p> 시스템 코드는 사용자 변경 불가 처리  </p></blockquote>
<br>
<ol>
  <li>일반설정: OPC/ 비가동/ 자재투입</li>
  <li>제품라벨: 사용여부/ 라벨분할/ 묶음발행/ 인쇄방식/ 스캔방식</li>
  <li>박스라벨: 사용여부/ 포장방식/ 스캔방식</li>
</ol>
<br>

<h2>그리드 조회</h2>
<br>
<blockquote><p> 초기1회만 조회하도록 적용  </p></blockquote>
<br>
<br>


<h2> 다국어 공통사이트 적용 </h2>
<br>
<blockquote><p> 사이트별 관리 => 공통관리 변경  </p></blockquote>
<br>
<br>


