---
layout: post
comments: true
title:  2017-07-03 업그레이드 내역
date:   2017-07-03 00:00:01
categories: 공통
tag: upgrade
---

* content
{:toc}


기능 추가
------------------------

<h2>LOT 추적기능 추가</h2>
<br>
<blockquote><p> 아이템 자재투입 기능 설정 필요  </p></blockquote>
<br>
<ol>
  <li>작업완료: 자재장착 시 추적연계 가능</li>
  <li>생산LOT 화면에서 장착된 자재 조회 가능</li>
</ol>
<br>

<h2>LOT 분할기능 추가</h2>
<br>
<blockquote><p> 환경설정: 분할기능 설정 시 가능  </p></blockquote>
<br>
<ol>
  <li>아이템 스캔 후 분할 수량 입력 시 가능</li>
</ol>
<br>
